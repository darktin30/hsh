# Home Sweet Home

```
   .---------.       
   |.-------.|        _  _                                   _     _                  
   ||>hsh#  ||       | || |___ _ __  ___   ____ __ _____ ___| |_  | |_  ___ _ __  ___ 
   ||       ||       | __ / _ \ '  \/ -_) (_-< V  V / -_) -_)  _| | ' \/ _ \ '  \/ -_)
   |"-------'|       |_||_\___/_|_|_\___| /__/\_/\_/\___\___|\__| |_||_\___/_|_|_\___|
 .-^---------^-.       
 | ---~    ⊙ω⊙ |  
 "-------------'      
```

hsh (Home sweet home) is a bash script that automatically installs all packages and dotfiles I need to feel comfortable on a new linux system.  
The script supports multiple linux distros, like ubuntu, debian, arch and some more.

It is highly customized to my needs and will need modification for your best experience.  
Feel free to fork it and add your personal twist!

# Usage

```
git clone https://gitlab.com/darktin30/hsh.git
./hsh
```
